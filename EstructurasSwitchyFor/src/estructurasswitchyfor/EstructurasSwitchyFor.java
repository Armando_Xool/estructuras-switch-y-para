/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurasswitchyfor;

import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class EstructurasSwitchyFor {

    /**
     * @param args the command line arguments
     */
    static void imprimir (String sMensaje){
            System.out.println(sMensaje);
            
        }
         static void separador (){
             imprimir("-----------------------------------");
         }
          static void presentacion ( ){
              imprimir("Univercidad Autónoma de Campeche ");
              imprimir("Facultad de ingenieria");
              imprimir("Ingenieria en sistamas computacionales");
              imprimir("Lenguaje de programacion 1");
              imprimir(" Carlos Armando Xool Medina");
              imprimir("Matricula:63833");
              imprimir("\u001b[30m" + "2 A");
          }
          /*
    Crear una método en lea un número de tipo int y se solicite a otro método que valide el dato entre
    el 1 al 7 si el valor esta fuera de este rango regrese que es incorrecto y el nombre del día de la
    semana, como segundo parametro reciba una valor boleano donde true es semana inglesa (inicia
    domingo), false es semana laborable (inicia en lunes).
    */
          static void semana(){
              Scanner sc = new Scanner(System.in);
              imprimir("Introduce 1 si la semana es inglesa y 0 si es semana laborable");
              int x = sc.nextInt();
              imprimir("Introduce en numero de 1 al 7, corespondiente al dia que desea saber");
              int num = sc.nextInt();
               boolean semana = x==1;
               if (semana){
                     switch(num){
                   case 1:
                       imprimir("Domingo");
                       break;
                    case 2:
                        imprimir("Lunes");
                        break;
                    case 3: 
                        imprimir("Martes");
                        break;
                    case 4:
                        imprimir("Miercoles");
                        break;
                    case 5:
                        imprimir("Jueves");
                        break;
                    case 6:
                        imprimir("Viernes");
                        break;
                    case 7:
                        imprimir ("sabado");
                        break;
                    default:
                        imprimir("dia incorecto");
                  }            
                 }
               else {
                     switch(num){                  
                    case 1:
                        imprimir("Lunes");
                        break;
                    case 2: 
                        imprimir("Martes");
                        break;
                    case 3:
                        imprimir("Miercoles");
                        break;
                    case 4:
                        imprimir("Jueves");
                        break;
                    case 5:
                        imprimir("Viernes");
                        break;
                    case 6:
                        imprimir ("sabado");
                        break;
                    case 7:
                        imprimir("Domingo");
                        break;
                    default:
                        imprimir("dia incorecto");
                  }           
                       }
          }
          /*
          Imprima utilizando los ciclos for el arreglo de dos dimensiones de los personajes de
          Star War.
         */
            static void StarWar (){
                String[][] aPersonajes;
aPersonajes = new String[16][3];
aPersonajes[0][0] = "Luke Skywalker";
aPersonajes[0][1] = "172";
aPersonajes[0][2] = "male";
aPersonajes[1][0] = "R2-D2";
aPersonajes[1][1] = "96";
aPersonajes[1][2] = "n/a";
aPersonajes[2][0] = "C-3PO";
aPersonajes[2][1] = "167";
aPersonajes[2][2] = "n/a";
aPersonajes[3][0] = "Darth Vader";
aPersonajes[3][1] = "202";
aPersonajes[3][2] = "male";
aPersonajes[4][0] = "Leia Organa";
aPersonajes[4][1] = "150";
aPersonajes[4][2] = "female";
aPersonajes[5][0] = "Owen Lars";
aPersonajes[5][1] = "178";
aPersonajes[5][2] = "male";
aPersonajes[6][0] = "Beru Whitesun lars";
aPersonajes[6][1] = "165";
aPersonajes[6][2] = "female";
aPersonajes[7][0] = "R5-D4";
aPersonajes[7][1] = "97";
aPersonajes[7][2] = "n/a";
aPersonajes[8][0] = "Biggs Darklighter";
aPersonajes[8][1] = "183";
aPersonajes[8][2] = "male";
aPersonajes[9][0] = "Obi-Wan Kenobi";
aPersonajes[9][1] = "182";
aPersonajes[9][2] = "male";
aPersonajes[10][0] = "Yoda";
aPersonajes[10][1] = "66";
aPersonajes[10][2] = "male";
aPersonajes[11][0] = "Jek Tono Porkins";
aPersonajes[11][1] = "180";
aPersonajes[11][2] = "male";
aPersonajes[12][0] = "Jabba Desilijic Tiure";
aPersonajes[12][1] = "175";
aPersonajes[12][2] = "hermaphrodite";
aPersonajes[13][0] = "Han Solo";
aPersonajes[13][1] = "180";
aPersonajes[13][2] = "male";
aPersonajes[14][0] = "Chewbacca";
aPersonajes[14][1] = "228";
aPersonajes[14][2] = "male";
aPersonajes[15][0] = "Anakin Skywalker";
aPersonajes[15][1] = "188";
aPersonajes[15][2] = "male";
            for( int i=0; i<16; i++){                  
                for (int j=0; j<3; j++){
                    imprimir (aPersonajes[i][j] );
                }
                
            }

            } 
  /*
Pedir un número y leer n veces números, realizar la suma de los numeros, sacar el
promedio y determinar cual número introducido es el mayor y cual el menor, y la
distancia númerica entre ellos.      
  */
  static void perdir_numero(){
      double suma =0;
      double mayor=0;
      double menor=1000000000;
      double promedio = 0;
      double distancia =0;
      Scanner sc = new Scanner(System.in);
      imprimir("Cuantos numero vas a introducir");
      int n = sc.nextInt();
      for(int i=0; i<n; i++){
          imprimir(i+1 + "- Introduce un numero");
          double d = sc.nextInt();
           if(d>mayor){
              mayor=d;               
          }
          if(d<menor){
               menor =d;
                      }
           suma=suma+d;
           promedio = suma/n;
           distancia= mayor-menor;
  }
    imprimir("El numero menor es " + menor );
     imprimir("El numero mayor es " + mayor);
     imprimir("la suma es " + suma );
    imprimir("El promedio es "+ promedio);
     imprimir("La ditancia entre el menor y el mayor es "+ distancia);
    
}
  static void menu ( ){
        imprimir("1.- Saber el dia de la semana");
        imprimir("2.- Saber el personajes de star wars");
        imprimir ("3.- Saber la suma de numeros, promedio, numero mayor y el menor y la distancia entre ellos");
        imprimir("0.- Salir");
        Scanner sc = new Scanner(System.in);
        imprimir("Introdusca su opción");
        int opc = sc.nextInt();
        separador();
        submenu(opc);
        }
        static void submenu (int opc ){
            switch(opc){
                case 0:
                    imprimir("Hasta la vista");
                    
                    break;
                case 1:
                     imprimir("Saber el dia de la semana");
                     separador( );
                    semana();
                     break;
                case 2:
                    imprimir("Saber el personajes de star wars");
                    separador( );
                    StarWar ();
                    break;
                case 3:
                    imprimir("Saber la suma de numeros, promedio, numero mayor y el menor y la distancia entre ellos");
                    separador( );
                    perdir_numero();
                    break;
                default:
                imprimir("Opcion incorecta");
                separador( );
                menu();
            }
        }
  public static void main(String[] args){
      presentacion ( );
      separador();
      menu();
      
  }
}
